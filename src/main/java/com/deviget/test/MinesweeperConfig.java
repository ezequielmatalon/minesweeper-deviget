package com.deviget.test;

import static springfox.documentation.builders.PathSelectors.regex;
//import org.h2.server.web.WebServlet;
//import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class MinesweeperConfig {

	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("minesweeper").apiInfo(apiInfo()).select()
				.paths(regex("/minesweeper.*")).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Minesweeper documentation")
				.description("Documentation and test REST API for minesweeper").contact("ezequielmatalon@gmail.com")
				.license("GNU license").build();
	}

//	@Bean
//	public ServletRegistrationBean h2servletRegistration() {
//		ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
//		registration.addUrlMappings("/console/*");
//		return registration;
//	}
}
