package com.deviget.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.deviget.test.controller.dto.CellRequestDTO;
import com.deviget.test.controller.dto.CellsOpenedDTO;
import com.deviget.test.model.Level;
import com.deviget.test.service.MinesweeperService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/minesweeper")
public class MinesweeperController {

	@Autowired
	private MinesweeperService minesweeperService;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "resumeGame", nickname = "Resume game")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "Game id", required = true, dataType = "long", paramType = "query") })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = CellsOpenedDTO.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public CellsOpenedDTO resumeGame(@RequestParam(name = "id") Long id) {
		CellsOpenedDTO board = minesweeperService.getBoard(id);
		return board;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "startGame", nickname = "Start new game")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "level", value = "Difficulty", required = true, dataType = "long", paramType = "request body") })
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Created", response = Long.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Failure") })
	public Long startGame(@RequestBody Level level) {
		return minesweeperService.createBoard(level);
	}

	@PutMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "leftClick", nickname = "Left click/Open cell")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = CellsOpenedDTO.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public @ResponseBody CellsOpenedDTO leftClick(@RequestBody CellRequestDTO cell) {
		return minesweeperService.open(cell.getColumn(), cell.getRow(), cell.getGameId());
	}

	@PatchMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "rightClick", nickname = "Right click/flag")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = CellsOpenedDTO.class),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 404, message = "Not Found"),
			@ApiResponse(code = 500, message = "Failure") })
	public void rightClick(@RequestBody CellRequestDTO cell) {
		minesweeperService.flag(cell.getColumn(), cell.getRow(), cell.getGameId());
	}

	@DeleteMapping
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ApiResponses(value = { @ApiResponse(code = 405, message = "Method not allowed") })
	public void delete() {
	}

}
