package com.deviget.test.controller.dto;

import com.deviget.test.model.Cell;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CellResponseDTO extends CellDTO {

	private Integer value;

	public CellResponseDTO(Cell cell) {
		super(cell.getColumnNumber(),cell.getRowNumber());
		value = cell.getNeighborMines();
	}
	
	
}
