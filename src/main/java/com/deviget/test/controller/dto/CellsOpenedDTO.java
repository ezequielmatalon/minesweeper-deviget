package com.deviget.test.controller.dto;

import java.util.Set;

import com.deviget.test.model.Board;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CellsOpenedDTO {
	private Boolean gameFinished;
	private Boolean win;
	private Set<CellResponseDTO> cellsOpened;

	public CellsOpenedDTO(Board board, Set<CellResponseDTO> cells) {
		this.cellsOpened = cells;
		win = board.win();
		gameFinished = board.getGameFinished();
	}

}
