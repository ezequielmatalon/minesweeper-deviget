package com.deviget.test.exception;

public class InvalidMinesweeperArgument extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMinesweeperArgument(String message) {
		super(message);
	}

}
