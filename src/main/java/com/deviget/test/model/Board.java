package com.deviget.test.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Board implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "board", fetch = FetchType.LAZY)
	private List<Cell> cells;
	private Integer openToWin;
	private Level level;
	private Boolean gameFinished;

	public Board(Level lev) {
		this.level = lev;
		cells = generateBoard(lev);
		openToWin = (lev.getColumns() * lev.getRows()) - lev.getMines();
		gameFinished = false;
	}

	private List<Cell> generateBoard(Level lev) {
		Cell[][] elements = new Cell[lev.getColumns()][lev.getRows()];
		generateMines(lev, elements);
		fillBoard(elements);
		List<Cell> boardCells = new ArrayList<>();
		for (Cell[] cells : elements) {
			boardCells.addAll(Arrays.asList(cells));
		}
		return boardCells;
	}

	private void fillBoard(Cell[][] elements) {
		for (int col = 0; col < level.getColumns(); col++) {
			for (int row = 0; row < level.getRows(); row++) {
				if (elements[col][row] == null) {
					Cell cell = CellBuilder.create().board(this).column(col).row(row).boardElements(elements).neighbors(this)
							.build();

					elements[col][row] = cell;
				}
			}
		}

	}

	private void generateMines(Level lev, Cell[][] elements) {
		Integer minesLeftToBeCreated = lev.getMines();
		ThreadLocalRandom randomizer = ThreadLocalRandom.current();
		while (minesLeftToBeCreated > 0) {
			Integer mineRow = randomizer.nextInt(lev.getRows());
			Integer mineCol = randomizer.nextInt(lev.getColumns());
			if (elements[mineCol][mineRow] == null) {
				Cell mine = MineBuilder.create().board(this).column(mineCol).row(mineRow).build();
				elements[mineCol][mineRow] = mine;
				minesLeftToBeCreated--;
			}

		}
	}

	public Cell getCell(Integer column, Integer row) {
		return cells.stream().filter(cell -> cell.getColumnNumber().equals(column) && cell.getRowNumber().equals(row)).findFirst()
				.get();
	}

	public Boolean win() {
		return openToWin == 0;
	}

	public void decrementOpenToWin() {
		openToWin--;
	}

}
