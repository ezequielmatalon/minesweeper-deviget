package com.deviget.test.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Cell implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Integer MINE_VALUE = -1;

	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "board_id", nullable = false )
	private Board board;
	private Integer rowNumber;
	private Integer columnNumber;
	private Integer neighborMines;
	private Boolean flagged;
	private Boolean open;

	public Cell(Integer row, Integer column, Integer mineDistance, Board board) {
		this.rowNumber = row;
		this.columnNumber = column;
		neighborMines = mineDistance;
		open = false;
		flagged = false;
		this.board = board;
	}

	public boolean isMine() {
		return MINE_VALUE.equals(neighborMines);
	}

}