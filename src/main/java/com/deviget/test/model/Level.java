package com.deviget.test.model;

import lombok.Getter;

@Getter
public enum Level {
	BEGINNER(8, 8, 10), INTERMEDIATE(16, 16, 40), HARD(24, 24, 99);

	private Integer rows;
	private Integer columns;
	private Integer mines;

	private Level(Integer columns, Integer rows, Integer mines) {
		this.rows = rows;
		this.columns = columns;
		this.mines = mines;
	}

}