package com.deviget.test.repository;

import org.springframework.data.repository.CrudRepository;

import com.deviget.test.model.Board;

public interface BoardRepository extends CrudRepository<Board, Long> {

}
