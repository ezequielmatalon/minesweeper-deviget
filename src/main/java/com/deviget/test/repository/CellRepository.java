package com.deviget.test.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.deviget.test.model.Cell;

public interface CellRepository extends CrudRepository<Cell, Long> {

	@Query("SELECT c FROM Cell c WHERE c.rowNumber = :row and c.columnNumber = :column and c.board.id = :boardId")
	public Cell findCellByColumnAndRow(@Param("boardId") Long id, @Param("column") Integer column, @Param("row") Integer row);

	@Query("SELECT DISTINCT c FROM Cell c WHERE c.neighborMines=-1 and c.board.id= :boardId")
	public Set<Cell> findMines(@Param("boardId") Long id);

}
