package com.deviget.test.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deviget.test.controller.dto.CellResponseDTO;
import com.deviget.test.controller.dto.CellsOpenedDTO;
import com.deviget.test.exception.InexistantMinesweeperGame;
import com.deviget.test.exception.InvalidMinesweeperOperation;
import com.deviget.test.model.Board;
import com.deviget.test.model.Cell;
import com.deviget.test.model.CellHelper;
import com.deviget.test.model.Level;
import com.deviget.test.repository.BoardRepository;
import com.deviget.test.repository.CellRepository;

@Service
public class MinesweeperService {
	@Autowired
	private BoardRepository repo;
	@Autowired
	private CellRepository cellRepo;

	public Long createBoard(Level level) {
		Board board = new Board(level);
		board = repo.save(board);
		return board.getId();
	}

	public CellsOpenedDTO getBoard(Long id) {
		Board board = repo.findOne(id);
		initalizeCells(board);

		if (board == null) {
			throw new InexistantMinesweeperGame();
		}
		Set<CellResponseDTO> cells = getOpenedCells(board);
		return new CellsOpenedDTO(board, cells);

	}

	private void initalizeCells(Board board) {
		Hibernate.initialize(board.getCells());
	}

	public void flag(Integer column, Integer row, Long gameId) {
		Cell cell = cellRepo.findCellByColumnAndRow(gameId, column, row);

		if (cell == null) {
			throw new InexistantMinesweeperGame();
		}

		if (cell.getOpen()) {
			throw new InvalidMinesweeperOperation();
		}

		cell.setFlagged(!cell.getFlagged());
		cellRepo.save(cell);
	}

	public CellsOpenedDTO open(Integer column, Integer row, Long gameId) {
		Cell cell = cellRepo.findCellByColumnAndRow(gameId, column, row);

		if (cell == null) {
			throw new InexistantMinesweeperGame();
		}
		if (cell.getOpen()) {
			throw new InvalidMinesweeperOperation();
		}
		Set<CellResponseDTO> cells = openCell(cell);
		Board board = cell.getBoard();
		Boolean gameFinished = board.win() || cell.isMine();
		board.setGameFinished(gameFinished);
		repo.save(board);
		return new CellsOpenedDTO(board, cells);
	}

	private Set<CellResponseDTO> openCell(Cell cell) {
		if (cell.getOpen() || cell.getFlagged()) {
			throw new InvalidMinesweeperOperation();
		}

		Board board = cell.getBoard();
		if (cell.getNeighborMines() == 0) {
			Hibernate.initialize(board.getCells());
			initalizeCells(board);
			Set<Cell> openCellsRecursive = openCellsRecursive(new HashSet<>(), cell);
			Set<CellResponseDTO> returnedCells = openCellsRecursive.stream().map(c -> new CellResponseDTO(cell))
					.collect(Collectors.toSet());
			return returnedCells;
		}

		if (cell.isMine()) {
			return openMines(board);
		}

		cell.setOpen(Boolean.TRUE);
		board.decrementOpenToWin();
		return Collections.singleton(new CellResponseDTO(cell));
	}

	private Set<CellResponseDTO> openMines(Board board) {
		Set<Cell> mines = cellRepo.findMines(board.getId());
		Set<CellResponseDTO> minesResponse = new HashSet<>();
		for (Cell mine : mines) {
			mine.setOpen(Boolean.TRUE);
			minesResponse.add(new CellResponseDTO(mine));
		}
		cellRepo.save(mines);
		return minesResponse;
	}

	private Set<Cell> openCellsRecursive(Set<Cell> cells, Cell current) {
		if (current.isMine() || current.getFlagged()) {
			return cells;
		}

		if (!current.getOpen()) {
			current.setOpen(Boolean.TRUE);
			current.getBoard().decrementOpenToWin();
			current = cellRepo.save(current);
			cells.add(current);
		}
		
		if (current.getNeighborMines() == 0) {
			List<Cell> neighbors = CellHelper.getNeighbors(current.getBoard(), current.getRowNumber(), current.getColumnNumber());
			for (Cell cell : neighbors) {
				if (!cells.contains(cell)) {
					cells.addAll(openCellsRecursive(cells, cell));
				}
			}
		}

		return cells;

	}

	Set<CellResponseDTO> getOpenedCells(Board board) {

		return board.getCells().stream().filter(c -> c.getOpen()).map(c -> new CellResponseDTO(c))
				.collect(Collectors.toSet());
	}

}
